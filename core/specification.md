# Travel Rule Protocol

| Version | Date               | Description                                            |
| ------- | ------------------ | ------------------------------------------------------ |
| 1.0.0   | 19th August 2020   | Initial Public Version                                 |
| 1.0.1   | 10th December 2020 | Bug fix                                                |
| 1.1.0   | 8th February 2021  | Clarifications, bug fixes and consistency enhancements |
| 2.0.0   | 10th May 2021      | Addition of LNURL enhanced flow                        |

## Executive Summary

Inherently, involvement within the virtual asset ecosystem presents
higher risks related to money laundering and terrorist financing than
those found in the traditional world of FIAT currency. Primarily this is
due to the lack of transparency of the public keys, which are not
associated with any identity.

Whilst in the traditional world, there is proven infrastructure with
processes and tools designed to mitigate AML/CFT risks, given the
evolution of the virtual asset ecosystem, there is now a need to find
comparable solutions not only to address industry concerns but incoming
global regulations. Key operators such as custodians and market
participants must consider the following issues or gaps:

1. The current inability to identify Originators and/or Beneficiaries of transactions.
1. Cumbersome processes and governance to be put in place in order to confirm to
whom a defined public key belongs.

There are varying approaches across the industry to define a common set
of standards and to create a mechanism for the exchange of relevant
information including identity and associated addresses details all
using a common infrastructure, protocol and data set.

- [Travel Rule Protocol](#travel-rule-protocol)
  - [Executive Summary](#executive-summary)
  - [Introduction](#introduction)
  - [Goals](#goals)
    - [First Generation](#first-generation)
    - [Minimal, Workable, Pragmatic API](#minimal-workable-pragmatic-api)
    - [Minimise Fragmentation & Maximise Interoperation](#minimise-fragmentation--maximise-interoperation)
  - [Design Philosophy](#design-philosophy)
  - [Traditional and Enhanced flow](#traditional-and-enhanced-flow)
    - [Traditional](#traditional)
    - [Enhanced](#enhanced)
    - [Minimal Core Protocol with Extensions](#minimal-core-protocol-with-extensions)
  - [Technical implementation - Traditional](#technical-implementation---traditional)
    - [Core Protocol](#core-protocol)
    - [Headers](#headers)
      - [Sample Header](#sample-header)
    - [Address Query](#address-query)
      - [Request](#request)
      - [Responses](#responses)
    - [Transfer Notification](#transfer-notification)
      - [Request](#request-1)
      - [Responses](#responses-1)
    - [Extensions](#extensions)
      - [Scope](#scope)
      - [Caveats](#caveats)
        - [Conflicts between extensions](#conflicts-between-extensions)
        - [Promotions from extension to core](#promotions-from-extension-to-core)
        - [Fallback](#fallback)
        - [Core protocol additions](#core-protocol-additions)
          - [API-Extensions header](#api-extensions-header)
          - [Extension not supported response code](#extension-not-supported-response-code)
          - [Request body](#request-body)
  - [Technical implementation - Enhanced](#technical-implementation---enhanced)
    - [Extensions](#extensions-1)
    - [LNURL response data](#lnurl-response-data)
  - [Compliance with FATF Recommendation 16](#compliance-with-fatf-recommendation-16)
  - [Authoritative Version](#authoritative-version)
  - [References](#references)
  - [Founding Organisations](#founding-organisations)
    - [List of Contributors in Alphabetic Order by Organisation](#list-of-contributors-in-alphabetic-order-by-organisation)

## Introduction

The Travel Rule working group was primarily established to address a
specific recommendation issued by the Financial Action Task Force
("FATF"). Members of the working group include leading industry
participants from around the world who meet regularly to discuss and
agree on finding a common solution to meet the regulatory standards. The
information within this paper seeks to highlight a solution, referred
herein as the Travel Rule Protocol (TRP).

The TRP vision is to define a set of standards which will allow Virtual
Asset Service Providers (VASP - as defined as in footnote[^1] below)
to safely share with other trusted participants the identity of an
originator and beneficiary linked to a specific on chain transaction.
This set of standards aim to meet the requirements set by the FATF
Recommendation 16 (R16), commonly known as the Travel Rule defined as
below:

> R16 -- Countries should ensure that originating VASPs obtain and
> hold required and accurate originator information and required
> beneficiary information on virtual asset transfers, submit the
> above information to the beneficiary VASP or financial institution
> (if any) immediately and securely, and make it available on
> request to appropriate authorities. Countries should ensure that
> beneficiary VASPs obtain and hold required originator information
> and required and accurate beneficiary information on virtual asset
> transfers, and make it available on request to appropriate
> authorities. Other requirements of R16 (including monitoring of
> the availability of information, and taking freezing action and
> prohibiting transactions with designated persons and entities)
> apply on the same basis as set out in R16. The same obligations
> apply to financial institutions when sending or receiving virtual
> asset transfers on behalf of a customer.

It is intended that adoption of the TRP is royalty and license free.
However any fees, licenses related to implementation, service of
software providers etc will be the responsibility of the implementing
VASP including but not limited to document the legal and contractual
arrangement ensuring that any exchange of information is done in
compliance with applicable laws, e.g. GDPR.

The TRP has been incrementally developed by a self-selecting group of
leading industry participants from around the world, collaborating
openly and sharing to develop and evolve the solution. There is no
single central body or organisation behind the protocol, only
collaboration.

The TRP Working Group has a regular weekly minuted meeting with a set
agenda. There are other forums including chat rooms for more frequent
collaboration as well as email for more infrequent formal announcements.
The Working Group welcomes all contributions, ideas and input to the
design and implementation of the protocol. Industry participants who
wish to help developing the protocol, or adopt the protocol are
encouraged to register at [https://travelruleprotocol.org/](https://travelprotocol.org)

Any comments, corrections or suggestions on this version of the protocol
specification can also be captured via the feedback form at
[https://travelruleprotocol.org](https://travelruleprotocol.org)

## Goals

The goal of the TRP is to develop a first-generation, minimal, workable,
and pragmatic API specification for compliance with the FATF Travel Rule
in a timely manner, that also minimises fragmentation and maximises
interoperability.

### First Generation

Industry participants acknowledge that mainstream adoption of virtual
assets as an asset class will take time, and encouragingly there is
evidence that its evolution is progressing with increasing focus by
regulators and central banks imposing regulatory requirements. The TRP
aims to initiate one of the first steps on this journey by launching a
first-generation solution for compliance with the FATF Travel Rule.

### Minimal, Workable, Pragmatic API

The TRP has been developed with sufficient flexibility allowing
straightforward adoption for a wide range of industry participants. By
focusing closely on FATF R16 and limiting the scope of the solution, The
TRP aims to create a simple API specification for required information
exchange that will seamlessly integrate with existing business IT
frameworks.

### Minimise Fragmentation & Maximise Interoperation

There are a number of potentially more comprehensive Travel Rule
solutions in development across the industry participants. By focusing
closely on FATF R16 and limiting the scope of the solution, the TRP aims
to create a simple API specification for the information exchange that
is easy to implement and Integrate with existing business solutions. As
well as having an easy path to interoperate with other emerging industry
Travel Rule solutions as they mature. Any investment made should be
transferable to future industry solutions.

## Design Philosophy

The underlying design principles of Travel Rule Protocol:

1. Compliance with FATF R16
1. Data privacy: Bilateral exchange for minimal data leakage
1. Iterative and incremental with early MVP launch
1. Open for industry collaboration and adoption
1. Aligned with emerging industry standards such as IVMS101
1. Any investment made should be transferable to future industry
solutions
1. Each member controls the data that they share.

## Traditional and Enhanced flow

The application is exposes an API that can be integrated with other backend
systems (as part of transaction processing for instance) to
automatically check if an address is registered. There is no central
application or database, instead, it is an application that runs within
each VASP's IT framework.

This document outlines two work-flows.

1. One in which the user copy pastes a regular blockchain address to the
   originator VASP.
2. In the other the user copy pastes a different text to the originator VASP
   called a LNURL.

### Traditional

In this work-flow the end users copy pastes a blockchain address from the
beneficiary VASP to the originator VASP. This is what everybody is used to.

Two API endpoints are to be exposed by VASPs to perform:

1. an Address Query, i.e., is a given public address being managed by a given VASP
2. a Transfer Notification from one VASP to another including details of a
   present virtual asset transfer.

The originator VASP is expect to first call the Address Query. When the
beneficiary VASP confirms he owns the address the request can be followed up
with a Transfer Notification. Shortly before or after that final request the
onchain transaction should have been done.

![address query](address-query.png)

Diagram (1) Address Query from Originating VASP to Beneficiary VASP.

![transfer notification](transfer-notification.png)

Diagram (2) Transfer Notification from Originating VASP to Beneficiary VASP (see
JSON formatted payload later in the document).

### Enhanced

The traditional approach has a problem. When the originator VASP receives an
address from the end user the originator VASP has no idea to whom that address
belongs. The VASP needs to ask all known VASPs via the Address Query if the
address is owned by that VASP. Not only that, maintaining a list of know VASPs
and associated endpoints can be a significant burden. As a VASP you cannot
change the location of the API endpoints without also notifying your counter
parties of the change. You then also need to ensure the changes are
incorporated in the systems of all your VASPs. This is cumbersome and prone to
missed Travel Rule messages. This problem is referred to as the 'VASP
discoverability problem'.

Instead of receiving a blockchain address from the end user the originator VASP
could also receive a specially formatted URL from the user called a LNURL[^2]
This LNURL is a bech32 encoded URL which contains a unique URL. Once the LNURL
is decoded by the originating VASP a request is made to that URL and the onchain
address is obtained from the beneficiary VASP. From thereon the originator VASP
can proceed with the Transfer Notification as described under the traditional
flow.

### Minimal Core Protocol with Extensions

The first part of the goals of the TRP:

> "first generation, minimal, workable, and pragmatic API specification
> for compliance with the FATF Travel Rule in a timely manner"

is centered around having a simple minimal protocol that is easy to
implement and to comply with.

The second part of the goals is:

> "minimise fragmentation and maximise interoperation"

In order to meet both of these goals the TRP is separated into two
parts:

1. A minimal core protocol
1. Extensions

The minimal core protocol should contain mandatory elements that cover
the bare minimum FATF R16 requirements. This meets the first part of the
goals.

However, it is acknowledged that one size does not fit all, taking into
account differing jurisdictional and compliance requirements on what
information is to be exchanged. To help accommodate the second goal an
extension mechanism is proposed.

The minimum core protocol will comply with the mandatory elements of
R16, all other requirements will be added using the extension mechanism.

## Technical implementation - Traditional

### Core Protocol

Each VASP who wants to be compliant with this specification must expose two
RESTful endpoints. In addition, and entirely optional, zero or more extensions
can be supported. Users of the TRP protocol should agree on a bilateral basis
on which extensions they will use.

### Headers

Each request must have the following header fields:

| Field                | Description                                                                                                         | Example value                                  |
| -------------------- | ------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------- |
| `api-version`        | Version of the API                                                                                                  | `1.0.0`                                        |
| `api-extensions`     | Comma separated list of extensions to the base the TRP protocol to be used when processing this request.[1](#note1) | `request-signing,beneficiary-details-response` |
| `request-identifier` | Unique UUID V4 value for this particular API call (different for all calls)                                         | `2351f3f1-bcff-4a06-a07a-8de94220a9b0`         |

<a name="note1">1</a>: Use of extensions is  optional and must be agreed between parties. A
server should be able to inform  a client it is unable or unwilling to support
the extensions as defined in the  client request's `api-extensions` header. Two
HTTP response codes are  applicable, the `422 - Unprocessable Entity` and the
`501 - Unimplemented` codes.  Either one of them is applicable in different
extension cases. See "Extensions"  section of the document for more details.

Each response must include the headers `api-version` and `request-identifier`.
The former holds the version of the API the responder is using, the latter
echos the `request-identifier` back to the sender.

#### Sample Header

```txt
api-version: 1.0.0
api-extensions: request-signing,beneficiary-details-response
request-identifier: 2351f3f1-bcff-4a06-a07a-8de94220a9b0
```

### Address Query

This service, when called by the originating VASP, requests if given address
 is known and is under management by the beneficiary VASP receiving the call.

#### Request

`GET /assets/{assetIdentifier}/addresses/{address}`

| Parameter         | Description                                     |
| ----------------- | ----------------------------------------------- |
| `assetIdentifier` | Digital asset type (ETH, BTC, ...). [1](#note2) |
| `address`         | Public address to query                         |
<a name="note2">1</a>: Note this must be sufficient to identify the token when
combined with the address (eg: USDT on Omni vs ERC20 USDT)

Example: `/assets/ETH/addresses/0x5eD8Cee6b633AD92f4fDB8fAd9F`

#### Responses

- Response HTTP 200: Address is managed by the beneficiary VASP who
    accepts transfer
- Response HTTP 404: Otherwise. The reason not to differentiate
    between an address not found and an unwillingness to accept a follow
    up Transfer Notification is to minimize data leakage. The
    Originator VASP when receiving a 404 is none the wiser with regard
    if the beneficiary controls the address or not.

### Transfer Notification

This service is for the Originating VASP to notify the Beneficiary VASP that a
virtual asset transfer has been executed on a distributed ledger. Upon
successful reception of the request the Beneficiary VASP must respond with a 200
OK status code.

Transfer Notification is meant for single party beneficiaries and single party
originators. If a virtual asset transfer is sent to multiple parties, the
Originating VASP would have to issue Transfer Notification to each Beneficiary
VASP, sharing the same blockchain transaction hash. Allowing only for single
parties runs counter to the IVMS101 spec which has provisions for multiple
parties. Going with single parties only is in line with the goal to create a
MVP.

#### Request

`POST /assets/{assetIdentifier}/transfers`

| Parameter         | Description                                     |
| ----------------- | ----------------------------------------------- |
| `assetIdentifier` | Digital asset type (ETH, BTC, ...). [1](#note2) |

The body is a JSON encoding of the IVMS101 data standard. See example below:

```json
{
  "IVMS101": {
    "originator": {
      "originatorPersons": [
        {
          "naturalPerson": {
            "name": {
              "nameIdentifier": [
                {
                  "primaryIdentifier": "Post",
                  "secondaryIdentifier": "Johnny",
                  "nameIdentifierType": "LEGL"
                }
              ]
            },
            "geographicAddress": [
              {
                "addressType": "GEOG",
                "streetName": "Potential Street",
                "buildingNumber": "123",
                "buildingName": "Cheese Hut",
                "postcode": "91361",
                "townName": "Thousand Oaks",
                "countrySubDivision": "California",
                "country": "US"
              }
            ],
            "customerIdentification": "1002390"
          }
        }
      ],
      "accountNumber": [
        "10023909"
      ]
    },
    "beneficiary": {
      "beneficiaryPersons": [
        {
          "naturalPerson": {
            "name": {
              "nameIdentifier": [
                {
                  "primaryIdentifier": "MachuPichu",
                  "secondaryIdentifier": "Freddie",
                  "nameIdentifierType": "LEGL"
                }
              ]
            }
          }
        }
      ],
      "accountNumber": [
        "1BVMFfPXJy2TY1x6wm8gow3N5Amw4Etm5h"
      ]
    },
    "originatingVASP": {
      "legalPerson": {
        "name": {
          "nameIdentifier": [
            {
              "legalPersonName": "VASP A",
              "legalPersonNameIdentifierType": "LEGL"
            }
          ]
        },
        "nationalIdentification": {
          "nationalIdentifier": "506700T7Z685VUOZL877",
          "nationalIdentifierType": "LEIX"
        }
      }
    }
  },
  "extensions": {
    "exampleExtensionName2": {
      "exampleExtensionField1": "exampleExtensionField1Value",
      "exampleExtensionField2": "exampleExtensionField2Value"
    }
  }
}
```

#### Responses

Response HTTP 200 to acknowledge reception of transfer details.

### Extensions

#### Scope

Extensions can add any and all HTTP headers or parameters.

The body of the requests can be added to the designated location.

#### Caveats

##### Conflicts between extensions

With the number of extensions growing the risk of conflicts or
incompatibilities between extensions grows exponentially new extension
authors should take special care to investigate and address any
conflicts.

##### Promotions from extension to core

Extensions might prove a comfortable testing ground for new
functionality. When an extension is universally used it could be
promoted to the core protocol. The promotion mechanism is still to be
decided.

##### Fallback

Implementations should cater to as many extensions as possible. Some
form of auto discovery and if practical fallback should be catered for.

##### Core protocol additions

In order to support extensions some extension-specific handling is
required in the core protocol beyond what is necessary for R16
compliance. These are described in the following section.

###### API-Extensions header

This header contains a comma separated list of predefined extension
names. It must be omitted when no extensions are enabled.

###### Extension not supported response code

A server should be able to inform a client it is unable or unwilling to
support the extensions as defined in the client request's
`api-extensions` header. Two HTTP response codes are applicable, the `422 -
Unprocessable Entity` and the `501 - Not Implemented`. Either one of them
is applicable in different extension cases.

###### Request body

Extensions have a reserved top level key in JSON payloads called `extensions`.
Each extension would then have a key below that with a specific payload. For
example:

```txt
{
  "IVMS101": ...
  "extensions": {
    "deterministicValueTransfer": {
      "txId": ...,
      "voutIndex": ...,
      "amount": ...,
    }
  }
}

```

When no extensions are used in the request, i.e. the `api-extensions` header is
absent, the key `extensions` must be omitted.

## Technical implementation - Enhanced

In the enhance flow one additional public endpoint is required of the
implementing VASP. It should be an endpoint which responds to a GET request but
is otherwise unconstrained. It's an implementation detail whether or not this
endpoint is unique and/or ephemeral. It is this endpoint that gets encoded in
the LNURL which is handed to the Beneficiary User.

![img](enhanced_flow.png)

1. Beneficiary User requests deposit “address” from beneficiary VASP.
2. Beneficiary VASP provides LNURL. In it is encoding a unique URL.
3. The Beneficiary User gives the LNURL to the Originating User.
4. Originating User uses Originating VASP’s withdrawal page/service to request
   withdrawal and provides LNURL.
5. Originating VASP decodes the LNURL and makes a call to the URL encoded at
   beneficiary VASP[^3].
6. Beneficiary VASP responds with `travelRuleRequest` tagged JSON payload. See below.
7. Originating VASP verifies details returned by the beneficiary VASP.
8. Originator VASP makes the onchain transfer
9. Originator VASP does the Transfer Notification

Step 8 and 9 (onchain transfer and the sending of the Transfer Notification) can be reversed.

The beneficiary user might be the same as the originator user.

An example LNURL looks like this:

```
LNURL1DP68GURN8GHJ7UM9WFMXJCM99E3K7MF0V9CXJ0M385EKVCENXC6R2C35XVUKXVS89HM
```

It encodes:

```
https://service.com/api?q=3fc3645b439c
```

### Extensions

When the Beneficiary VASP expects the Originator VASP to use extensions he can
append a list of extensions as a query parameter to the encoded URL. This is
counter to what the traditional flow is doing in which extensions are
communicated by headers. The LNURL specification is
[quite clear](https://github.com/fiatjaf/lnurl-rfc#http-status-codes-and-content-type)
that that status codes and HTTP headers carry no meaning. They do not here too.

### LNURL response data

```json
{
    "version": "1.2.0",
    "api-extensions": "exampleExtensionName1,exampleExtensionName2",
    "extensions": {
      "exampleExtensionName2": {
        "exampleExtensionField1": "exampleExtensionField1Value",
        "exampleExtensionField2": "exampleExtensionField2Value"
      }
    },
    "asset": "BTC",
    "address": "bc1q5pucatprjrqltdp58f92mhqkfuvwpa43vhsjwpxlryude0plzyhqjkqazp",
    "tag": "travelRuleRequest"
}

```

`"version"`: Version of Travel rule supported by the Beneficiary VASP.

`"api-extensions"`: The extensions supported in this response.

`"extensions"`: Any and all data related to the used extensions. This is an
object with 1 top level key for each extension. The value of that keys can be
anything. Note that if an extension does not carry additional data it can be
omitted.

`"asset"`: The asset to which the address belongs.

`"address"`: The address to which the Originator VASP is expected to send the funds to.

`"tag"`: A static `"travelRuleRequest"` value.

## Compliance with FATF Recommendation 16

The key part of the FATF VASP Recommendations is:

> A.  As described in INR.15, paragraph 7(b), all of the requirements set
> forth in Recommendation 16 apply to VASPs or other obliged
> entities that engage in VA transfers, including the obligations to
> obtain, hold, and transmit required originator and beneficiary
> information in order to identify and report suspicious
> transactions, monitor the availability of information, take
> freezing actions, and prohibit transactions with designated
> persons and entities.

*By exchanging the Originator and Beneficiary information between VASPs,
the TRP facilitates compliance with this requirement. Information must
be stored by each VASPs themselves. No central or distributed database
is proposed.*

> A.  Further, countries should ensure that beneficiary institutions
> (whether a VASP or other obliged entity) obtain and hold required
> (not necessarily accurate) originator information and required and
> accurate beneficiary information, as set forth in INR. 16. The
> required information includes the:
>
> 1. originator's name (i.e., the sending customer);
*The TRP facilitates compliance with this requirement.*
> 1. originator's account number where such an account is used to process
> the transaction (eg: the VA wallet); *The TRP facilitates compliance with
>  this requirement, The account is the public address.*
> 1. originator's physical (geographical) address, or national identity
> number, or customer identification number (i.e., not a transaction
> number) that uniquely identifies the originator to the ordering
> institution, or date and place of birth; *The TRP facilitates compliance with
>  this requirement. The TRP Working Group has agreed to use LEI or GEOG from
> IVMS101 in first phase*
> 1. beneficiary's name; and *The TRP facilitates compliance with this requirement.*
> 1. beneficiary account number where such an account is used to process
> the transaction (eg: the VA wallet). It is not necessary for the
> information to be attached directly to the VA transfer itself. The
> information can be submitted either directly or indirectly, as set
> forth in INR. 15. *The TRP facilitates compliance with this requirement, The
> account is the public address.*

## Authoritative Version

This document provides a description of the TRP and its
usage. Where possible this document will be updated and new versions
issued as the protocol is revised.

## References

- GDF Taxonomy for Cryptographic Asset Proofs
  [https://www.gdf.io/wp-content/uploads/2018/10/0010\_GDF\_Taxonomy-for-Cryptographic-Assets\_Proof-V2-260719.pdf](https://www.gdf.io/wp-content/uploads/2018/10/0010_GDF_Taxonomy-for-Cryptographic-Assets_Proof-V2-260719.pdf)
- IVMS101: Joint Working Group for interVASP Messaging Standards
  [www.intervasp.org](http://www.intervasp.org)
- FATF Guidance for a Risk-Based Approach to Virtual Assets and
  Virtual Asset Service Providers, particularly points 111 to 119 in
  the context of the TRP:
  [https://www.fatf-gafi.org/publications/fatfrecommendations/documents/guidance-rba-virtual-assets.html](https://www.fatf-gafi.org/publications/fatfrecommendations/documents/guidance-rba-virtual-assets.html)

## Founding Organisations

The Travel Rule Protocol working group was founded by and the first
draft version of the TRP protocol was created by Standard Chartered, ING
and BitGo. Through welcoming open industry collaboration the working
group has since grown to encompass a large number of leading
organisations from across the global Virtual Asset Industry and
continues to welcome further organisations to join.

The working group is currently chaired by Andrew Davidson, CTO of OSL.

[^1]: See definition of Virtual Asset Service Providers at <https://www.fatf-gafi.org/glossary/u-z/>
[^2]: <https://github.com/fiatjaf/lnurl-rfc>

### List of Contributors in Alphabetic Order by Organisation

| Name                 | Organisation                         |
| -------------------- | ------------------------------------ |
| Harm Aarts           | 21 Analytics AG                      |
| Lucas Betschart      |                                      |
| Filip Gospodinov     |                                      |
| Adedeji Owonibi      | A&D Forensics                        |
| Andrew Davidson      | BC Group / OSL                       |
| Hugh Madden          |                                      |
| Nathan Simmons       |                                      |
| Usman Ahmad          |                                      |
| Ben Chan             | BitGo                                |
| Chris Metcalfe       |                                      |
| Kiarash Mosayeri     |                                      |
| Dave Jevans          | CipherTrace                          |
| John Jefferies       |                                      |
| Charlene Cieslik     | Complifact AML Inc.                  |
| Magdalena Boškić     | Crypto Finance AG                    |
| Nathaniel Zollinger  |                                      |
| Malcolm Wright       | Diginex / EQUOS.io                   |
| Ray Hennessey        |                                      |
| Jack Gavigan         | Electric Coin Company                |
| David Carlisle       | Elliptic                             |
| James Byrne          | Digivault                            |
| Sheehan Anderson     | Fidelity Digital Assets              |
| Ekaterina Anthony    | Geissbühler Weber & Partner (gwp)    |
| Ben El-Baz           | HashKey                              |
| Alessio Quaglini     | Hex Trust                            |
| Rafal Czerniawski    |                                      |
| Simon Lee            | Hodlnaut Pte. Ltd.                   |
| Antoine Girard       | ING                                  |
| Herve Francois       |                                      |
| Kaloyan Tanev        |                                      |
| Susan Patterson      | Komainu (Jersey) Limited             |
| Benjamin Usinger     | KPMG Advisory                        |
| Edmund Lowell        | KYC-Chain                            |
| Adrien Treccani      | Metaco                               |
| Thomas Hardjono      | MIT Connection Science & Engineering |
| Justin Newton        | Netki                                |
| Andrés Junge         | Notabene                             |
| Pelle Braendgaard    |                                      |
| Megan Monroe-Coleman | OKCoin                               |
| Alexandre Kech       | Onchain Custodian                    |
| Raymond              |                                      |
| Javier Tamashiro     | Ospree                               |
| Jukka Pellinen       |                                      |
| Lana Schwartzman     | Paxful                               |
| Peter Davey          | Peter Davey and Associates Limited   |
| Jo Lee               | Standard Chartered Bank              |
| Maxime de Guillebon  |                                      |
| Thierry Janaudy      |                                      |
| Duncan Trenholme     | TP ICAP                              |
| Simon Forster        |                                      |
| Dave Jevans          | Trisa                                |
| John Jefferies       |                                      |
| Neil Samtani         | XReg                                 |
| Siân Jones           |                                      |

**End of Document**
