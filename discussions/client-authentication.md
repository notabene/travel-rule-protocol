# Client authentication

TLS allows the client to know to which server it is sending a request to. In our
case, the server also needs to know to whom it is responding because of the 
sensitive data involved. Only known VASPs are allowed to communicate with 
each other.

## Client side certificates

This is an extension of server side certificates. When establishing a 
connection, not only does the client verify the identity of the server but, the 
server can verify the identity of the client too.

### Pros

1. Authentication stays at the transport layer
1. Established standard

### Cons

1. Authentication stays at the transport layer
1. Cumbersome to use
1. Hard to generate a secure certificate (we need to tell what settings to use)
1. Certificate management
