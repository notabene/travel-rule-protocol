# TRP Extension: Message Signing

## Goal

This extension provides a mechanism for VASPs to exchange messages with a
[Ed25519](https://ed25519.cr.yp.to) signature as proof of non-repudiation.
It can be applied to all message types in the Travel Rule Protocol including:

-   Address Query
-   Transfer Notification

Ed25519 is pick for it's favourable characteristics.

## Extension header name

In line with v1 on the spec the name would be: `message-signing`. The
`api-extensions` header would look like:

```
api-extensions: message-signing,some-other-extension
```

## Other headers

In addition to the other TRP headers two more are added:

1. `public-key`: The base64 encoded Ed25519 public key of sender
2. `signature`: The base64 encoded signature of the request or response

## Message to sign

The signer will sign the concatenation of:

1. path: path, query parameters and query fragments.
2. request/response body: as raw bytes.
3. HTTP status code: as a number and only if applicable.

## Examples

For an address query the signed request looks like this (nonsense public key
and signature):

```
API-Extensions: message-signing,some-other-extension
Public-Key: AAAAC3NzaC1lZDI1NTE5AAAAIMBt0uy0i38pHoBlE3cWCXRd/SwReffNt5HXVnMPJVbl
Signature: y1z6KgGK58wb5G60JOJgXumb3Hmjb5yO51ess4dDw268I10bEaBtQz/rch9m\n1aU8E+qwNHb5n9+K6cIxAWYCDg==
```

The signed content would be
`/assets/{assetIdentifier}/addresses/{address}`.

The response would include the same headers with a different `public-key` and
`signature` value.
The signed content would be
`/assets/{assetIdentifier}/addresses/{address}200`.
